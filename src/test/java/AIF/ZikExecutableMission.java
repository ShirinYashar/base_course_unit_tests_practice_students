package AIF;

import AIF.Missions.MissionTypeException;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class ZikExecutableMission {
    public static AIFUtil aifUtil;
    @BeforeClass
    public static void setUpBeforeClass(){
        aifUtil = new AIFUtil();
    }


    @Test//Executable Mission -> BdaMission
    public void testZikExecutableMission() throws MissionTypeException {
        try{
            aifUtil.getAerialVehiclesHashMap().get("Zik").setMission(aifUtil.getAllMissions().get("bda"));
            assertTrue(true);
        }catch (MissionTypeException missionTypeException){//if exception is thrown it is mean that mission cannot be deploy with the curen aerial vehicle.
            fail();

        }
    }

    @Test//Executable Mission -> AttackMission
    public void testZikUnexecutableMission() throws MissionTypeException{
        try{
            aifUtil.getAerialVehiclesHashMap().get("Zik").setMission(aifUtil.getAllMissions().get("attack"));
            fail();
        }catch (MissionTypeException missionTypeException){
            assertTrue(true);
        }
    }
}
